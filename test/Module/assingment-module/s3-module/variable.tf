variable "s3-my_module" {
    default = "module-1254"
    type = string
}

variable "tags" {
  description = "statetf.file get stored"
  type        = map(string)
  default = {
    Name        = "Name"
    Environment = "Dev"
    Project     = "backed_terraform"
  }
  sensitive = false
}