variable "ami" {
    default = "ami-00beae93a2d981137"
    type = string 

}

variable "instance_type" {
    default = "t2.micro"
    type = string 

}



variable "ec2_ebs" {
    type = map(string)
    default = {
        device_name = "/dev/xvdba"
        volume_size = 10
        volume_type = "gp3"
    
    }


}

variable "tags" {
    default = {
        Name        = "Name"
        Environment = "Dev"
        Project     = "backed_terraform"

    }
  
}