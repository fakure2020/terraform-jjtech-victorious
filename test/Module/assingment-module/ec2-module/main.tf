
resource "aws_instance" "ec2_module" {
  ami           = var.ami
  instance_type = var.instance_type
  availability_zone = "us-east-1a"

  ebs_block_device {
    device_name = var.ec2_ebs.device_name 
    volume_size = var.ec2_ebs.volume_size
    volume_type = var.ec2_ebs.volume_type
  }

  tags = var.tags
    
  
}





