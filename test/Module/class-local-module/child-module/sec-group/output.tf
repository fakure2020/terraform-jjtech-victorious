output "sg_ids" {
    value = [aws_security_group.webserver.id]
    description = "The ID of the security group"
}