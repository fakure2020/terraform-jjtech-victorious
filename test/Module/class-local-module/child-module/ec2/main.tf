resource "aws_instance" "web_server" {
  ami = var.ami_bliss_id
  key_name = var.key_name
  vpc_security_group_ids = var.sg_ids
  subnet_id = var.subnet_id
  instance_type = var.instance_type
  tags = var.tags
}



