variable "cidr" {
    type = string
    description = "provide cide block for vpc"
    default = "10.0.0.0/16"
  
}

variable "vpc_tags" {
  type = map(string)
  description = "provide tags for vpc"
  default = {
    Name = "my-vpc"
  }
  
}

variable "subnet_cidr" {
    type = string
    description = "porvide cidr block for sbunet"
    default = "10.0.0./24"
  
}