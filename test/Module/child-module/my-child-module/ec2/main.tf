resource "aws_instance" "name" {
  count = var.do-you-want-crate-ec2-instance ? 5 : 0
    ami = var.ami
    instance_type = var.instance-type
  tags = {
    "Name" = "jtech-module"
  }
}