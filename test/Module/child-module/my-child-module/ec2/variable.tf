variable "ami" {
    type = string
    description = "value for ami"
    default = "ami-022e1a32d3f742bd8"
}

variable "instance-type" {
  type = string
  description = "value for instance type"
}

variable "do-you-want-crate-ec2-instance" {
  type = bool
}

